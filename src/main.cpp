#include <iostream>
#include <string>
#include <fstream>
using std::string;
using std::cout;
using std::cin;
using std::endl;
using std::ifstream;
using std::ofstream;
/**
 * Entry point
 * 
 * 
 * @param argc Arguments count
 * @param argv Arguments
 */
int main(int argc, char **argv) {
    //printf("Hello,%s!\n",argv[1]);
    string buf (argv[1]);
    if ( buf == "write") {
        ofstream file (argv[2]);
        file<<argv[3];
        file.close();
        cout<<"success"<<endl;
    }
    
    else if ( buf == "read" ) {
        ifstream file (argv[2]);
        if(!file.is_open())
            cout<<"not found"<<endl;
    
        else {
            file >> buf;
            file.close();
            cout << buf << endl;
        }
    }
    else if ( buf == "delete") {
        remove(argv[2]);
        cout<<"success"<<endl;
    }
       
return 0;
}
